package gaiasky.render;

import gaiasky.scenegraph.particle.BillboardDataset;

public interface IBillboardDatasetProvider {

    public BillboardDataset[] getDatasets();
}
