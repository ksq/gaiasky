package gaiasky.render;

public enum BlendMode {
    ADDITIVE,
    ALPHA
}
