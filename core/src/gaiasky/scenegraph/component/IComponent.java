package gaiasky.scenegraph.component;

public interface IComponent {
    void initialize(String name);
}
