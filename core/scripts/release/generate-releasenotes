#!/bin/bash

# This script generates the releasenotes.txt file that goes with
# every release

# Utils dir, with a relative path core/utils/ to the root gaiasky directory
UTILSDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"
GSDIR=`realpath $UTILSDIR/../../../`

if [[ $# -ne 2 ]]; then
    echo "Usage: $0 VERSION OUTPUT"
    echo
    echo "$0: Must provide a version (tag) number and an output file."
    exit -1
fi

# The current tag
CURRENT_TAG=$1
OUTPUT=$2
RELEASENOTES=$GSDIR/releasenotes.txt

echo "Creating release notes: $CURRENT_TAG"
# Test tag for regexp, if it does not fit, use $RELEASENOTES if it exists
RE="^([0-9]+\.)?([0-9]+\.)?([0-9]+)$"

if [[ ! $CURRENT_TAG =~ $RE ]]; then
    echo "Tag $CURRENT_TAG does not match $RE, trying to use $RELEASENOTES"
    if [[ -e $RELEASENOTES ]]; then
        # Use it
        echo "$GSDIR/releasenotes.txt -> $OUTPUT"
        cp $GSDIR/releasenotes.txt $OUTPUT
    else
        echo "File $RELEASENOTES does not exist"
    fi
else
    # Use regexp to match x.y.z[-rcww]
    echo "git-chglog -> $OUTPUT"
    git-chglog --tag-filter-pattern '^(\d+\.)?(\d+\.)?(\*|\d+)?$' --config $GSDIR/.chglog/config.rn.yml -o $OUTPUT $CURRENT_TAG && echo "File saved to $OUTPUT"
fi
echo "Done"
